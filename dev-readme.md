# Working with cabal
--------------------


#### Use cabal always in sandbox mode!!!! Otherwise, modules are installed globally.


### Sandbox: Basic usage

Fresh start:

```
$ cd /pat/to/module
$ cabal sandbox init
$ cabal install --only-dependencies
$ cabal build
```
You need to have already initialized cabal project with ```$ cabal init```.

Now everything you install will be installed in sandbox mode. 

e.g. ```$ cabal install persistent``` 


Next, check your ```.cabal``` file and add installed module under ```build-depends``` indicating supported version.

Remember to ```$ cabal build``` your project after every change of cabal files. 

Also, you can run your project with ```$ cabal repl```.

