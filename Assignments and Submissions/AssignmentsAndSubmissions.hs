module AssignmentsAndSubmissions  ( listSubmissions
                                  , getSubmission
                                  , createAssignment
                                  , getConfiguration
                                  , listFiles
                                  , getSubmissionPath
                                  ) where

import Data.Char
import Data.Text (Text(), Text(..))
import Data.List 
import Data.List.Split
import Data.Time 
import Data.Text (unpack, pack)
import System.Directory
import Control.Monad

-- | Academic year shorthand (e.g. 2015 for 2015/16)
type Year = Integer

-- | An assignment type
data Type = Homework | Exam | Project deriving Show

-- | A an assignment configuration data structure
-- | Uses Data.Time.UTCTime
-- | If files is an empty list, ANY number of files are OK  
data Configuration = Configuration  { published :: UTCTime -- When to publish
                                    , deadline :: UTCTime -- Submission deadline
                                    , lateDeadline :: UTCTime -- Late submission deadline
                                    , files :: [String] -- File names to expect
                                    , minScore :: Double -- Minimum achievable
                                    , maxScore :: Double -- Maximum achievable
                                    , required :: Double -- Score req to pass
                                    -- Anything else you might think would be useful
                                    } deriving Show

-- | An assignment descriptor 
data Assignment = Assignment  { year :: Year
                              , type' :: Type
                              , number :: Int
                              } deriving Show

data Submission = Submission  { userIdentifier :: UserIdentifier
                              , submittedFiles :: [String]
                              , assignment     :: Assignment
                              } deriving Show


-- | A user identifier (not DB id) like a username or JMBAG
type UserIdentifier = String

storageRoot = "/Users/patrik/Desktop"

-- ==== test variables ========================================================
time1 = read "2015-12-15 18:00:00.00 UTC" :: UTCTime
time2 = read "2015-12-30 18:00:00.00 UTC" :: UTCTime
time3 = read "2016-01-27 18:00:00.00 UTC" :: UTCTime
conf = Configuration time1 time2 time3 ["Homework.hs","Excersise.hs"] 00.0 100.0 50.0
a1 = Assignment 2015 Homework 5
s1 = Submission "Branko" ["Homework.hs"] a1

-- ==== end of variables ========================================================


-- ==== Assignments API part ========================================================

-- | Creates a new assignment from Assignment, configuration and PDF file
-- | The PDF file should be copied, moved or symlinked so that it is
-- | accessible from the assignment directory.
createAssignment :: Assignment -> Configuration -> FilePath -> IO ()
createAssignment a c fp = do 
  makeDirectoryStructure $ getAssignmentPath a
  saveConfiguration a c fp
  copyFile fp $ (getAssignmentPath a) ++ "Assignment.pdf"
    --where getExtension = last . splitOn "." 
    --      getFileName = last . splitOn "/" 

-- | Creates a new .config file. 
saveConfiguration :: Assignment -> Configuration -> FilePath -> IO () 
saveConfiguration a c fp = writeFile ((getAssignmentPath a) ++ ".config") (configToString c) 


-- | Gets the configuration object for an assignment
getConfiguration :: Assignment -> IO Configuration
getConfiguration a = do
  c <- readFile $ (getAssignmentPath a) ++ ".config"
  return $ stringToConfig c


-- * helper functions 

-- | Converts Configuration to a simple config structure
configToString :: Configuration -> String
configToString (Configuration p d l f mn mx r) 
  = unlines [ "published = " ++ (show p)
            , "deadline = " ++ (show d)
            , "lateDeadline = " ++ (show l)
            , "files = " ++ (show f)
            , "minScore = " ++ (show mn)
            , "maxScore = " ++ (show mx)
            , "required = " ++ (show r) ]

-- | Converts simple config structure to a Configuration data type
stringToConfig :: String -> Configuration
stringToConfig s = foldr (toConfPar) conf x
  where x = ((map (splitOn " = ")) . lines) s 
        toConfPar ("published":x:_)     conf = conf { published    = (read x :: UTCTime) }
        toConfPar ("deadline":x:_)      conf = conf { deadline     = (read x :: UTCTime) }
        toConfPar ("lateDeadline":x:_)  conf = conf { lateDeadline = (read x :: UTCTime) }
        toConfPar ("files":x:_)         conf = conf { files        = (read x :: [String]) }
        toConfPar ("minScore":x:_)      conf = conf { minScore     = (read x :: Double) }
        toConfPar ("maxScore":x:_)      conf = conf { maxScore     = (read x :: Double) }
        toConfPar ("required":x:_)      conf = conf { required     = (read x :: Double) }

getAssignmentPath :: Assignment -> FilePath
getAssignmentPath (Assignment y t n) = intercalate "/" [storageRoot, lowerize y, lowerize t, lowerize n, ""]
  where lowerize x = (map toLower) $ show x

makeDirectoryStructure :: FilePath -> IO ()
makeDirectoryStructure f = do
  createDirectoryIfMissing True f


getDirectoryContents' :: FilePath -> IO [String]
getDirectoryContents' x = do
  y <- getDirectoryContents x
  return $ removeThisAndParent y
  where removeThisAndParent = filter (\x -> x /= "." && x /= "..")

-- ==== Submissions API part ========================================================

-- | Given a solution file body, adds a solution directory/file to the
-- | directory structure of an assignment. It will indicate an error
-- | (using Maybe, exceptions, or some other mechanism) if the file is
-- | not in a defined permitted list. It will override already made
-- | submissions.
-- | Assignment -> File Body -> File Name -> Error indication (?)
upload :: Assignment -> UserIdentifier -> Text -> String -> IO (Maybe Submission)
upload a uID t s = do
  c <- getConfiguration a
  case s `elem` (files c) of
    False -> return Nothing
    True  ->  do 
      makeDirectoryStructure savePath
      writeFile (savePath ++ s) (unpack t)
      return (Just $ Submission uID [s] a)
        where savePath = getAssignmentPath a ++ uID ++ "/" 

isDirectory :: FilePath -> IO Bool
isDirectory x = do
  p <- getPermissions x
  return $ searchable p

-- | Lists the user identifiers for submissions made for an assignment
listSubmissions :: Assignment -> IO [UserIdentifier]
listSubmissions a = do 
  content <- getDirectoryContents' path
  r <- filterM (isDirectory) (getFullPath content)
  return $ map (lastFolder) r
    where lastFolder = last . splitOn "/"
          path = getAssignmentPath a
          getFullPath = map (path ++)


-- | Views a single submission in detail
getSubmission :: Assignment -> UserIdentifier -> IO Submission
getSubmission a uID = do
  content <- getDirectoryContents' (path ++ uID)
  let files = filter (not . (isPrefixOf "review-")) content
  return (Submission uID files a)
    where path = getAssignmentPath a


-- | Lists the files contained in a submission
listFiles :: Submission -> IO [FilePath]
listFiles s = do
  return $ submittedFiles s

-- | Computes a file path for a submission
getSubmissionPath :: Submission -> FilePath
getSubmissionPath s = ((getAssignmentPath . assignment) s) ++ userIdentifier s ++ "/"



