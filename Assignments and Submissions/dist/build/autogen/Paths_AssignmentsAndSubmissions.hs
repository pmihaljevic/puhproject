module Paths_AssignmentsAndSubmissions (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/patrik/Developer/Haskell/Project/Assignments and Submissions/.cabal-sandbox/bin"
libdir     = "/Users/patrik/Developer/Haskell/Project/Assignments and Submissions/.cabal-sandbox/lib/x86_64-osx-ghc-7.10.2/AssignmentsAndSubmissions-0.1.0.0-CSzL09vaT3oKA9ZnQwW8kc"
datadir    = "/Users/patrik/Developer/Haskell/Project/Assignments and Submissions/.cabal-sandbox/share/x86_64-osx-ghc-7.10.2/AssignmentsAndSubmissions-0.1.0.0"
libexecdir = "/Users/patrik/Developer/Haskell/Project/Assignments and Submissions/.cabal-sandbox/libexec"
sysconfdir = "/Users/patrik/Developer/Haskell/Project/Assignments and Submissions/.cabal-sandbox/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "AssignmentsAndSubmissions_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "AssignmentsAndSubmissions_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "AssignmentsAndSubmissions_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "AssignmentsAndSubmissions_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "AssignmentsAndSubmissions_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
