{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module User 
  ( createUser
  , deleteUser
  , getUser
  , isRoleInYear
  , listUsers
  , listUsersInRole
  , updateUser ) where

import Control.Monad.IO.Class   (liftIO, MonadIO)
import Data.Text                (Text)
import Database.Persist
import Database.Persist.Sqlite  
import Database.Persist.TH

-- | A user identifier (not DB id) like a username or JMBAG
type UserIdentifier = String

-- | The user’s role in the course
data Role = Student Integer -- Academic Year shorthand (2015 for 2015/16)
          | TA Integer Integer -- AY shorthand range (inclusive)
          | Professor
          deriving (Eq, Ord, Show, Read)

-- | A user (the definition can be bigger)
data User = User { 
  identifier  :: UserIdentifier,
  email       :: String,
  pwdHash     :: String,
  role        :: Role } deriving (Eq, Show)


share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
DbUser
  identifier  UserIdentifier
  email       String
  pwdHash     String
  role        String
  deriving Eq Show

|]


main :: IO ()
main = runSqlite "database.db" $ do
  runMigration migrateAll
  return ()


-- | Takes a user identifier, e-mail, password and role.
-- | Performs password hashing and stores the user into the
-- | database, returning a filled User. If creating it fails (e.g.
-- | the user identifier is already taken), throws an appropriate
-- | exception.
createUser :: UserIdentifier -> String -> String -> Role -> IO User
createUser uID email pass r = runSqlite "database.db" $ do
  let user = User uID email pass r
  user1 <- selectFirst [DbUserIdentifier ==. uID] []
  case user1 of
    Nothing -> insert (DbUser uID email pass (show r))
    Just _  -> error "User with same identifier already exists"
  
  return user


-- | Deletes a user referenced by identifier. If no such user or the
-- | operation fails, an appropriate exception is thrown.
deleteUser :: UserIdentifier -> IO ()
deleteUser uID = runSqlite "database.db" $ do
  user <- selectFirst [DbUserIdentifier ==. uID] []
  case user of
    Nothing -> error "No user with given identifier"
    Just _  -> deleteWhere [DbUserIdentifier ==. uID]


-- | Fetches a single user by identifier
getUser :: UserIdentifier -> IO User
getUser uID = runSqlite "database.db" $ do
  user <- selectFirst [DbUserIdentifier ==. uID] []
  case user of
    Nothing -> error "No user with given identifier"
    Just x  -> return $ (dbUserToUser . entityVal) x


-- | Checks whether the user has a role of AT LEAST X in a given academic
-- | year.
isRoleInYear :: User -> Role -> Integer -> Bool
isRoleInYear (User _ _ _ r1@(Student x))  r2@(Student y)  z = z > x && r1 <= r2
isRoleInYear (User _ _ _ r1@(TA x1 x2))   r2@(TA y1 y2)   z = x1 <= z && z <= x2 && r1 <= r2
isRoleInYear (User _ _ _ (Professor))     (Professor)     _ = True
isRoleInYear _                            _               _ = False


-- | Lists all the users
listUsers :: IO [User]
listUsers = runSqlite "database.db" $ do
  test <- selectList [DbUserIdentifier >. ""] []
  return $ dbUsersToUsers test



-- | Lists all users in a given role
listUsersInRole :: Role -> IO [User]
listUsersInRole r = runSqlite "database.db" $ do
  test <- selectList [DbUserRole ==. (show r)] []
  return $ dbUsersToUsers test


-- | Updates a given user. Identifies it by the UserIdentifier (or
-- | maybe database id field, if you added it) in the User and overwrites
-- | the DB entry with the values in the User structure. Throws an
-- | appropriate error if it cannot do so; e.g. the user does not exist.
updateUser :: User -> IO ()
updateUser (User uID e p r)   = runSqlite "database.db" $ do
  user <- selectFirst [DbUserIdentifier ==. uID] []

  case user of
    Nothing -> error "No user with given identifier"
    Just x  -> update (entityKey x) [DbUserEmail =. e, DbUserPwdHash =. p, DbUserRole =. (show r)]



-- ======== helper functions 
userToDbUser :: User -> DbUser
userToDbUser (User i e p r) = DbUser i e p (show r) 

dbUserToUser :: DbUser -> User
dbUserToUser (DbUser i e p r) = User i e p (read r :: Role) 

dbUsersToUsers :: [Entity DbUser] -> [User]
dbUsersToUsers [] = []
dbUsersToUsers (x:xs) = (dbUserToUser . entityVal) x : (dbUsersToUsers xs)
