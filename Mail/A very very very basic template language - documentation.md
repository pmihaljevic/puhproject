# A very very very basic template language

This language is used to convert a simple mail template into a piece of text ready to be sent off via your favourite webmail provider.
The user is provided a plethora of options while writing his/her template (you get variables and if-else statements, be creative).

#### Syntax
Variables are written in double curly braces with no spaces between the variable name and the braces.
``` sh
{{variable_name}}
```

Conditional statements are limited to simple if-else statements and are written in _postfix (Reverse Polish) notation_. All keywords (if, else, endif) start with $. The statement is written inside of round brackets with no whitespaces between the brackets and the actual statement. The author may use basic logical operators, such as *not*, *or* or *and* (written in this manner).
``` sh
$if(statement)
    foo
$else
    bar
$endif


Example:
---------
$if(isProf isTA or)
    I'm employed here!
$else
    I'm only a student...
$endif
```

#### Available functions

*A very very basic template language* is currently supporting following functions:

* **isStud** - Returns true if the current user is a student
* **isTA** - Returns true if the current user is a teaching assistant
* **isProf** - Returns true if the current user is a professor