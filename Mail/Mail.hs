{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Mail (

) where

import Control.Monad
import Data.Aeson
import Data.List
import Data.Text.Lazy (pack, unpack, Text, toStrict, fromStrict, empty, splitOn, replace)
import Data.Word
import Network.Mail.Mime (Address(..))
import Network.HaskellNet.SMTP (authenticate, AuthType(LOGIN), closeSMTP, sendMimeMail2)
import Network.HaskellNet.SSL (defaultSettingsWithPort)
import Network.HaskellNet.SMTP.SSL (connectSMTPSTARTTLSWithSettings)
import Network.Mail.SMTP (htmlPart, Password, renderAndSend, simpleMail, UserName)
import Network.Socket

import qualified Data.ByteString as BS
import qualified Data.Map.Strict as Map

import GHC.Generics (Generic)

-- ************** TESTING *****************************************************
-- | A user identifier (not DB id) like a username or JMBAG
type UserIdentifier = String

-- | The user’s role in the course
data Role = Student Integer -- Academic Year shorthand (2015 for 2015/16)
          | TA Integer Integer -- AY shorthand range (inclusive)
          | Professor
          deriving (Eq, Ord, Show, Read)

-- | A user (the definition can be bigger)
data User = User {
  identifier  :: UserIdentifier,
  email       :: String,
  pwdHash     :: String,
  role        :: Role } deriving (Eq, Show)

sampleUser = User {identifier = "0036485656", email = "test@test.com", pwdHash = "1234", role = Student 2014}

-- ***** E-mail sample ********
to :: [String]
to = ["floreani.filip@gmail.com", "filip.floreani.1995@gmail.com"]
-- ****************************************************************************


-- | FromJSON Configuration instance
instance FromJSON Configuration

-- | Template error implementation
data TemplateError = MiscError String deriving (Show, Eq, Read)

-- | SMTP server configuration
data Configuration = Configuration {
  host    :: HostName,
  port    :: Int,
  sender  :: String,
  user    :: UserName,
  pwd     :: Password } deriving (Show, Generic)

-- | An alias for the template contents
type Template = Text

-- | Expression operators
binOps :: Map.Map String (Bool -> Bool -> Bool)
binOps = Map.fromList [ ("or", (||)), ("and", (&&))]
unOp :: (String, (Bool -> Bool))
unOp = ("not", not)

-- | Template values
valueMap :: Map.Map String String
valueMap = Map.fromList [("{{firstName}}", "Kylo"), ("{{userName}}", "Supreme Leader Snoke")]


main :: IO ()
main = do
  putStrLn "Enter mail template file (HTML format)"
  templateFile <- getLine

  smtpConf <- readConfig
  strTemplate <- readFile templateFile

  let textTemplate = pack strTemplate
      mailTemplate = checkOutput $ compileTemplate textTemplate valueMap

  sendMail smtpConf mailTemplate to

-- | Parses an expression and returns either a result or an error
-- | message, if the parsing fails. If a variable is undefined, you
-- | can either return an error or replace it with an empty string.
-- | You can use a more elaborate type than String for the Map values,
-- | e.g. something that differentiates between Strings and Bools.
compileTemplate :: Template -> Map.Map String String -> Either TemplateError Template
compileTemplate template values
  | null $ unpack template = Left (MiscError "The template is empty")
  | null values = Left (MiscError "There are no values")
  | otherwise = Right $ fillTemplate template (Map.toList values)

-- | Sends an e-mail with given text to list of e-mail addresses
-- | using given configuration. Throws appropriate error upon failure.
sendMail :: Configuration -> Template -> [String] -> IO ()
sendMail smtp content addr = do
  putStrLn "Connecting to mail server..."
  conn <- connectSMTPSTARTTLSWithSettings hostname (defaultSettingsWithPort portNum)

  putStrLn "Authenticating..."
  authSucceed <- authenticate LOGIN username password conn
  if authSucceed
    then putStrLn "Login successful, sending email..."
    else error "Authentication failed"
  if authSucceed
    then sendMimeMail2 mail conn
    else error "Failed to send mail"
  closeSMTP conn
  where
    mail = simpleMail from (sToA addr) [] [] "" [(htmlPart content)]
    from = Address Nothing (toStrict $ pack $ sender smtp)
    hostname = host smtp
    portNum = fromIntegral (port smtp) :: PortNumber
    username = user smtp
    password = pwd smtp


-- | Reads the e-mail configuration object from a file, using some
-- | default path to config file.
readConfig :: IO Configuration
readConfig = do
  putStrLn "Enter SMTP server config file (JSON format)"
  jsonFile <- getLine

  jsonBS <- BS.readFile jsonFile
  let conf = decodeStrict jsonBS
  case conf of
    Nothing -> error "Malformed JSON file, failed to decode"
    Just x -> return x


-- ======== Helper actions =========
-- | Fills the variable holders in the template with values from
fillTemplate :: Template -> [(String, String)] -> Template
fillTemplate template [] = filterByConds template
fillTemplate template ((k, v) : t) =
  fillTemplate (replace (pack k) (pack v) template) t

-- | Returns a new template according to the condition expressions
filterByConds :: Template -> Template
filterByConds template = evalHelper (lines $ unpack template) False (-1) []
  where
    evalHelper [] _ _ acc = pack $ unlines $ reverse acc
    evalHelper (h : t) cond isElse acc
      | "$if" `isPrefixOf` h =
        if evaluateCond (words $ stripChars "()" (drop 3 h))
          then
            evalHelper t True 0 acc
          else
            evalHelper t False 0 acc
      | "$else" `isPrefixOf` h =
        evalHelper t cond 1 acc
      | "$endif" `isPrefixOf` h =
        evalHelper t False (-1) acc
      | otherwise =
        if cond
          then
            if isElse == 0 then
              evalHelper t cond isElse (h : acc)
            else
              evalHelper t cond isElse acc
          else
            if isElse == 0 then
              evalHelper t cond isElse acc
            else
              evalHelper t cond isElse (h : acc)

-- | Evaluates a single condition written in RPN
evaluateCond :: [String] -> Bool
evaluateCond expr = condEval expr []
  where
    condEval [] acc = head acc
    condEval (arg : expr) acc
      | arg == "isStud" = condEval expr ((isStud $ role sampleUser) : acc)
      | arg == "isTA" = condEval expr ((isTA $ role sampleUser) : acc)
      | arg == "isProf" = condEval expr ((isProf $ role sampleUser) : acc)
      | isBinOper arg && (not $ null $ tail acc) =
        condEval expr ((((Map.!) binOps arg) (acc !! 1) (head acc)) : (drop 2 acc))
      | isUnOper arg && (not $ null acc) =
        condEval expr (( (snd unOp) (head acc)) : (drop 1 acc))
      | otherwise = False
      where
        isUnOper op = op == (fst unOp)
        isBinOper op = Map.member op binOps

-- | If input is an error, returns empty text,
-- | else returns the template
checkOutput :: Either TemplateError Template -> Text
checkOutput (Left _) = ""
checkOutput (Right txt) = txt

-- | Removes given chars from given string
stripChars :: String -> String -> String
stripChars = filter . flip notElem

-- | Converts a list of strings to a list of addresses
sToA :: [String] -> [Address]
sToA [] = []
sToA (h : t) = (Address Nothing (toStrict $ pack h)) : sToA t

-- ======== Template language functions ========
isStud :: Role -> Bool
isStud (Student _) = True
isStud _ = False

isTA :: Role -> Bool
isTA (TA _ _) = True
isTA _ = False

isProf :: Role -> Bool
isProf Professor = True
isProf _ = False
-- =============================================
