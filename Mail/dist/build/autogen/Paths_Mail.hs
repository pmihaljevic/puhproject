module Paths_Mail (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/patrik/Library/Haskell/bin"
libdir     = "/Users/patrik/Library/Haskell/ghc-7.10.2-x86_64/lib/Mail-0.1.0.0"
datadir    = "/Users/patrik/Library/Haskell/share/ghc-7.10.2-x86_64/Mail-0.1.0.0"
libexecdir = "/Users/patrik/Library/Haskell/libexec"
sysconfdir = "/Users/patrik/Library/Haskell/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "Mail_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "Mail_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "Mail_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "Mail_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "Mail_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
